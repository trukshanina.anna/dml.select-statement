/*Which staff members made the highest revenue for each store and deserve a bonus
for the year 2017? */

-- 1 вариант
select distinct on (s.store_id)
  s.store_id, p.staff_id, s.first_name, s.last_name, sum(p.amount) as total_revenue
from staff s
  join payment p using (staff_id)
  join rental r using (rental_id)
where
  extract(year from p.payment_date) = 2017
group by s.store_id, p.staff_id, s.first_name, s.last_name
order by s.store_id, total_revenue desc;


 -- 2 вариант
select store_id, staff_id, first_name, last_name, total_revenue
from (
    select s.store_id, p.staff_id, s.first_name, s.last_name,
        sum(p.amount) as total_revenue,
        rank() over (partition by s.store_id 
					 order by sum(p.amount) desc) as rank_per_store
    from staff s
        join payment p using (staff_id)
        join rental r using (rental_id)
    where date_part('YEAR', p.payment_date) = 2017
    group by s.store_id, p.staff_id, s.first_name, s.last_name
) as ranked_data
where
    rank_per_store = 1; 
 
 
 -- вариант 3 
select store_id, staff_id, first_name, last_name, total_revenue
from (
    select s.store_id, p.staff_id, s.first_name, s.last_name,
        sum(p.amount) as total_revenue,
         row_number() over ( partition by s.store_id 
							order by sum(p.amount) desc) as rank_per_store
    from staff s, payment p, rental r
    where s.staff_id = p.staff_id
        and p.rental_id = r.rental_id
        and to_char(p.payment_date, 'YYYY') = '2017'
    group by s.store_id, p.staff_id, s.first_name, s.last_name
) as ranked_data
where
    rank_per_store = 1;
	
	
/*Which five movies were rented more than the others, and what is the expected 
age of the audience for these movies?*/

-- 1 вариант
select f.film_id, f.title, f.rating, count(r.rental_id) as rental_count
from film f
  join inventory i using (film_id)
  join rental r using (inventory_id)
group by f.film_id, f.title, f.rating
order by rental_count desc limit 5;


-- 2 вариант
select f.film_id, f.title, f.rating,
    (select count(rental_id) from rental r where r.inventory_id in 
	 (select i.inventory_id FROM inventory i where i.film_id = f.film_id)) as rental_count
from film f
group by f.film_id, f.title, f.rating
order by rental_count desc limit 5;


/*Which actors/actresses didn't act for a longer period of time than the others?*/

-- 1 вариант
select a.first_name, a.last_name, max(f.release_year) as latest_year 
from actor a
join film_actor fa on a.actor_id = fa.actor_id
join film f on fa.film_id = f.film_id
group by a.first_name, a.last_name
order by latest_year asc;

-- 2 вариант
select a.first_name, a.last_name, (select max(f.release_year)
from film_actor fa
join film f on fa.film_id = f.film_id
where fa.actor_id = a.actor_id) 
as latest_year
from actor a 
order by latest_year asc;